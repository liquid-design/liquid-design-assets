# LIQUID DESIGN SYSTEM

Boosting Digital Products at Merck KGaA, Darmstadt, Germany
A compendium of guidelines & assets for designers and React / React Native components for developers.Created by the team UX Strategy & Design at theChief Digital Organization of Merck KGaA, Darmstadt, Germany.


__Save Time & Money__

By using the Liquid Design System you will have shorter onboarding times, fewer feedback loops and an overall quicker production phase.


__Built with the User in Mind__

With many years of experience in the fields of UX and UI design, Liquid Design System has been crafted by designers and developers to ensure a high usability and the best possible experience for our users.

You can find more information at https://emd.design
